import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AgmCoreModule } from '@agm/core';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCYaNfRlKcJOiyxvU01DJKf-74Ew7JNZGU',
      libraries: ['drawing']
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
