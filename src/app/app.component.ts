import { Component, Input, Output } from '@angular/core';
import { MouseEvent } from '@agm/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'GeoPal Test';

  iconUrl = 'http://maps.google.com/mapfiles/ms/icons/';
  currentIcon = 'http://maps.google.com/mapfiles/ms/icons/green-dot.png';
  lat = 43.879078;
  lng = -103.4615581;
  selectedMarker;
  icons = [
    { color: 'Green', url: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'},
    { color: 'Blue', url: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'},
    { color: 'Pink', url: 'http://maps.google.com/mapfiles/ms/icons/pink-dot.png'},
  ];
  markers = [
    { lat: 22.33159, lng: 105.63233, alpha: 1, icon: this.currentIcon },
    { lat: 7.92658, lng: -12.05228, alpha: 1, icon: this.currentIcon },
    { lat: 48.75606, lng: -118.859, alpha: 1, icon: this.currentIcon },
    { lat: 5.19334, lng: -67.03352, alpha: 1, icon: this.currentIcon },
    { lat: 12.09407, lng: 26.31618, alpha: 1, icon: this.currentIcon },
    { lat: 47.92393, lng: 78.58339, alpha: 1, icon: this.currentIcon }
  ];
  zones = [];
  tempMarkers = [];
  tempIcon: '';
  
  nestedPaths = [[
    { lat: 0, lng: 10 },
    { lat: 0, lng: 20 },
    { lat: 10, lng: 20 },
    { lat: 10, lng: 10 },
    { lat: 0, lng: 10 }
  ]];

  useLasso = false;
  
  public files: Set<File> = new Set();

  addMarker(event) {
    console.log(event);
    if(typeof event.coords === 'undefined'){
      var lat = event[0];
      var lng = event[1];

    }else{
      var lat = event.coords.lat;
      var lng = event.coords.lng;
    }

    // this.zones.push({lat, lng});
    this.markers.push({ lat, lng, alpha: 1, icon: this.currentIcon });

    console.log(this.zones);
  }


  max(coordType: 'lat' | 'lng'): number {
    return Math.max(...this.markers.map(marker => marker[coordType]));
  }

  min(coordType: 'lat' | 'lng'): number {
    return Math.min(...this.markers.map(marker => marker[coordType]));
  }

  onClickInfoView(event){
    console.log(event);
  }

  selectMarker(event) {
    console.log(event);
    this.selectedMarker = {
      lat: event.latitude,
      lng: event.longitude
    };
  }

  changeColor(e){

    this.tempIcon = e.target.value;

  }


  onFilesAdded(event) {
    var file = event.srcElement.files[0];
    var json;

    if (file) {
      var reader = new FileReader();
      reader.readAsText(file, "UTF-8");
      reader.onload = function (evt) {
        // this.markers = [];
        json = JSON.parse(evt.target['result']).features;

        this.tempMarkers = json;



      }.bind(this);
      reader.onerror = function (evt) {
        console.log('error reading file');
      }
    }
  }



  changeIcon(e){
    var icon = this.tempIcon;
    this.currentIcon = icon;

    this.markers.forEach(marker => {
      marker.icon = icon;
      console.log(marker);
    });
  }


  jsonToMap(){
    this.markers = [];
    var sets = this.tempMarkers;
    sets.forEach(co => {
      co.geometry.coordinates.forEach(coords => {
        this.addMarker(coords);
      });
    });
  }


  ngOnInit() {
    console.log('App ready...');
  }

}
